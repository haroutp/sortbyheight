﻿using System;
using System.Collections.Generic;

namespace SortByHeight
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] n = new int[8]{-1, 150, 190, 170, -1, -1, 160, 180};
            int[] n1 = sortByHeight(n);
            foreach (var item in n1)
            {
                System.Console.Write(item + " ");
            }
        }

        public static int[] sortByHeight(int[] a) {
            int[] newA = new int[a.Length];
            List<int> subA = new List<int>();

            foreach (var item in a)
            {
                if(item != -1){
                    subA.Add(item);
                }
            }

            subA.Sort();

            for (int i = 0; i < a.Length; i++)
            {
                if(a[i] == -1){
                    newA[i] = a[i];
                }else{
                    newA[i] = subA[0];
                    subA.RemoveAt(0);
                }
            }
            return newA;
        }

    }
}
